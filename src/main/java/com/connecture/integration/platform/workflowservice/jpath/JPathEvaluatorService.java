package com.connecture.integration.platform.workflowservice.jpath;

import com.connecture.integration.platform.workflowcommon.ApplicationConstants;
import com.connecture.integration.platform.workflowcommon.InvalidConfigurationException;
import com.connecture.integration.platform.workflowcommon.model.Configuration;
import com.connecture.integration.platform.workflowcommon.model.WorkflowStep;
import com.connecture.integration.platform.workflowcommon.service.BaseService;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Preconditions;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import org.apache.camel.CamelContext;
import org.apache.camel.Message;
import org.apache.camel.TypeConversionException;
import org.apache.camel.TypeConverter;
import org.apache.camel.builder.xml.XPathBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

@Component
public class JPathEvaluatorService extends BaseService
{
  protected static final String HEADER_NAME = "HeaderName";
  protected static final String JSON_PATH_EXPRESSION = "JsonPathExpression";

  @Override
  public void doConfigure() throws Exception
  {
    from(getDirectEndpoint())
        .routeId(getRouteId())
        .process(exchange -> {
          Message in = exchange.getIn();
          WorkflowStep step = in.getHeader(
              ApplicationConstants.Headers.WORKFLOW_STEP, WorkflowStep.class);
          Preconditions.checkNotNull(step);
          Preconditions.checkArgument(step.getDependencies().size() == 1,
              "This service only supports a single dependency");
          getLogger().debug("WorkflowStep: " + step);

          Object previousResponse =
              step.getDependencies().get(0).getWorkflowStep().getServiceResponse();

          CamelContext ctx = exchange.getContext();
          TypeConverter typeConverter = ctx.getTypeConverter();
          Configuration config = step.getServiceConfiguration();
          String jpathExpression = config.get(JSON_PATH_EXPRESSION);
          String input = null;
          try
          {
            Document document = typeConverter.convertTo(Document.class, previousResponse);
            Element element = typeConverter.convertTo(Element.class, document);
            input = XPathBuilder.xpath("//text()", String.class).evaluate(ctx, element);
          }
          catch (TypeConversionException e)
          {
            // Ignore, as it can be json string payload
          }

          if (input == null)
          {
            input = typeConverter.convertTo(String.class, previousResponse);
          }
          DocumentContext documentContext = JsonPath.using(com.jayway.jsonpath.Configuration .defaultConfiguration()
              .jsonProvider(new JacksonJsonNodeJsonProvider())
              .mappingProvider(new JacksonMappingProvider())
              .addOptions(Option.DEFAULT_PATH_LEAF_TO_NULL))
              .parse(input);

          JsonNode node = documentContext
              .read(jpathExpression, JsonNode.class);

          String result = node.toString();

          String headerName = config.get(HEADER_NAME);
          exchange.getIn().setHeader(headerName, result);

          getLogger().debug(
              String.format(
                  "Set header '%s' to '%s'", headerName, result));

          exchange.getIn().setBody(previousResponse);
        });

  }

  @Override
  public String getServiceId()
  {
    return "JPathEvaluatorService";
  }

  @Override
  public void validateConfiguration(Configuration configuration) throws InvalidConfigurationException
  {
    Preconditions.checkNotNull(configuration);
    validateKeyExists(configuration, JSON_PATH_EXPRESSION);
    validateKeyExists(configuration, HEADER_NAME);
  }
}
