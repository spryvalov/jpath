package com.connecture.integration.platform.workflowservice.jpath;

import com.connecture.integration.platform.workflowcommon.ApplicationConstants;
import com.connecture.integration.platform.workflowcommon.model.Dependency;
import com.connecture.integration.platform.workflowcommon.model.WorkflowStep;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static com.google.common.base.Charsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = ("/META-INF/spring/test-bundle-context.xml"))
public class JPathEvaluatorServiceTest
{
  public static final String DIRECT_JSON_PATH = "direct:JPathEvaluatorService";
  public static final String TEST_HEADER = "testHeader";
  public static final String TEST_BODY = "test";
  @Autowired
  protected CamelContext context;
  @Autowired
  protected JPathEvaluatorService jPathEvaluatorService;
  @Produce(uri = DIRECT_JSON_PATH)
  protected ProducerTemplate template;

  private static final ObjectMapper MAPPER = new ObjectMapper();

  @Test
  public void shouldEvaluateJsonArrayFromJsonInput() throws Exception
  {
    String header = getExchange("samples/elasticsearch1.xml", "$.values")
        .getIn().getHeader(TEST_HEADER, String.class);

    assertNotNull(header);
    ArrayNode arrayNode = MAPPER.readValue(header, ArrayNode.class);
    assertEquals(2, arrayNode.size());
  }

  @Test
  public void shouldEvaluateJsonArrayFromDeliveryResponse() throws Exception
  {
    String header = getExchange("samples/elasticsearch1.json", "$.values")
        .getIn().getHeader(TEST_HEADER, String.class);

    assertNotNull(header);
    ArrayNode arrayNode = MAPPER.readValue(header, ArrayNode.class);
    assertEquals(2, arrayNode.size());
  }

  @Test
  public void shouldEvaluateValueFromDeliveryResponse() throws Exception
  {
    String header = getExchange("samples/deliveryResponse.xml", "$.groupId")
        .getIn().getHeader(TEST_HEADER, String.class);

    assertNotNull(header);
    assertEquals("26735", header);
  }

  private Exchange getExchange(String fileName, String jpathExpr) throws IOException
  {
    URL resource = JPathEvaluatorServiceTest.class.getClassLoader().getResource(fileName);
    String fileAsString = FileUtils.readFileToString(new File(resource.getFile()), UTF_8);

    WorkflowStep step = new WorkflowStep();
    WorkflowStep prevStep = new WorkflowStep();
    prevStep.setServiceResponse(fileAsString);
    step.getDependencies().add(new Dependency(prevStep));
    step.getServiceConfiguration().put("JsonPathExpression", jpathExpr);
    step.getServiceConfiguration().put("HeaderName", TEST_HEADER);
    Exchange exchange = context.getEndpoint(DIRECT_JSON_PATH).createExchange(ExchangePattern.InOut);
    exchange.getIn().setBody(TEST_BODY);
    exchange.getIn().setHeader(ApplicationConstants.Headers.WORKFLOW_STEP, step);
    template.send(DIRECT_JSON_PATH, exchange);

    return exchange;
  }

}